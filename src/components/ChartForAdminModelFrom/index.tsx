import React from 'react';
import {Button, message, Modal, Typography} from 'antd';
import {ModalForm} from "@ant-design/pro-form";
import MyCard from "@/components/Mychart";
import {deleteChartUsingPOST} from "@/services/icebi/chartController";

// 确认删除函数
const confirmDelete = async (id, onChartDeleted) => {
  try {
    const res = await deleteChartUsingPOST({id});

    if (res.code !== 0) {
      message.error('删除失败!');
      return false;
    }

    // 调用回调函数刷新列表项
    onChartDeleted();
    message.success('删除成功');
    return true;

  } catch (error) {
    console.error('删除图表时出错:', error);
    message.error('删除失败!!!');
    return false;
  }
};

// 删除操作函数
const handleDelete = (id, onChartDeleted) => {
  Modal.confirm({
    title: '确认删除',
    content: '确定要删除这个图表吗？',
    onOk: () => confirmDelete(id, onChartDeleted),
  });
};

// 图表详情组件
const ChartForAdminModelFrom = ({item, onChartDeleted}) => {

  return (
    <div style={{textAlign: 'right'}}>
      <div style={{display: 'flex', alignItems: 'center'}}>
        <Typography.Link
          key="delete"
          type="danger"
          style={{marginLeft: '10px', marginRight: '10px', bottom: '1'}}
          onClick={() => handleDelete(item.id, onChartDeleted)}>
          删除
        </Typography.Link>
        <ModalForm
          title="详情"
          trigger={<Typography.Link>详情</Typography.Link>}
          width="80%"
          submitter={{
            render: () => {
              return [
                <Button danger key="delete" onClick={() => handleDelete(item.id, onChartDeleted)}>
                  删除
                </Button>,
              ];
            }
          }}
          onFinish={async (values) => {
            console.log(values);
            message.success('删除成功');
            return true;
          }}
        >
          <div style={{marginBottom: 16}}/>
          <div>{'图表标题: ' + item.name}</div>
          <div>{'图表ID: ' + item.id}</div>
          <div>{'创建时间: ' + new Date(item.createTime).toLocaleString('zh-CN', {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
          })}</div>
          <div>{'分析目标：' + item.goal}</div>
          <div style={{marginBottom: 16}}/>
          {/*修复bug 把ReactECharts放入Card标签中*/}
          {/*出现bug的原因可能是，option的加载比div的加载速度快，导致第一次加载没有挂在到div里，再次点开，才被挂载到*/}
          {item.genChart && <MyCard genChart={item.genChart}/>}
          <div>{item.genResult ?? ''}</div>
        </ModalForm>
      </div>
    </div>
  );
};

export default ChartForAdminModelFrom;
