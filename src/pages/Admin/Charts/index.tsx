import type {ProColumns} from '@ant-design/pro-components';
import {PageContainer, ProTable} from '@ant-design/pro-components';
import '@umijs/max';
import {message, Space, Tag} from 'antd';
import React, {useEffect, useState} from 'react';
import {listChartByPageUsingPOST} from "@/services/icebi/chartController";
import ChartForAdminModelFrom from "@/components/ChartForAdminModelFrom";

/**
 * 图表管理页面
 *
 * @constructor
 */
const ChartsPage: React.FC = () => {
    const initSearchParams = {
      pageSize: 20,
      current: 1,
      sortField: 'createTime',
      sortOrder: 'desc',
    }
    const [searchParams, setSearchParams] = useState<API.ChartQueryRequest>({...initSearchParams});
    const [chartList, setChartList] = useState<API.Chart[]>([]);
    const [total, setTotal] = useState<number>(0);

    // 刷新数据的回调函数
    const handleChartDeleted = () => {
      loadData();
    };

    const loadData = async () => {
      try {
        const res = await listChartByPageUsingPOST(searchParams);
        if (res.data) {
          setChartList(res.data.records ?? []);
          setTotal(res.data.total ?? 0);
          // 隐藏图表的 title
          if (res.data.records) {
            res.data.records.forEach(data => {
              if (data.status === 'succeed') {
                const chartOption = JSON.parse(data.genChart ?? '{}');
                chartOption.title = undefined;
                data.genChart = JSON.stringify(chartOption);
              }
            })
          }
        } else {
          message.error('获取图表失败');
        }
      } catch (e: any) {
        message.error('获取用户图表失败' + e);
      }
    }

    useEffect(() => {
      loadData();
    }, [searchParams]); // 监听 searchParams 变化

    /**
     * 表格列配置
     */
    const columns: ProColumns<API.Chart>[] = [
      {
        title: '图表id',
        dataIndex: 'id',
        valueType: 'text',
        hideInForm: true,
        copyable: true,
        tooltip: 'id过长会自动收缩',
        ellipsis: true,
        width: '15%'
      },
      {
        title: '图表名称',
        dataIndex: 'name',
        valueType: 'text',
      },
      {
        title: '分析目标',
        dataIndex: 'goal',
        valueType: 'text',
        copyable: true,
        ellipsis: true,
        tooltip: '标题过长会自动收缩',
        formItemProps: {
          rules: [
            {
              required: true,
              message: '此项为必填项',
            },
          ],
        },
        width: '20%',
      },
      {
        title: '图表类型',
        dataIndex: 'chartType',
        valueType: 'text',
      }, {
        title: '图表状态',
        dataIndex: 'status',
        valueType: 'text',
        render: (_, record) => {
          let color = '';
          let statusText = ''; // 新增的变量，用于存储转换后的状态文本
          switch (record.status) {
            case 'succeed':
              color = 'green';
              statusText = '成功'; // 将 "succeed" 转换为 "成功"
              break;
            case 'running':
              color = 'blue';
              statusText = '运行中';
              break;
            case 'failed':
              color = 'red';
              statusText = '失败';
              break;
            case 'wait':
              color = 'yellow';
              statusText = '等待中';
              break;
            default:
              color = 'default';
              statusText = record.status; // 默认情况下保持原始状态文本
              break;
          }
          return <Tag color={color}>{statusText}</Tag>;
        },
      },
      {
        title: '创建者id',
        dataIndex: 'userId',
        valueType: 'text',
        copyable: true,
        tooltip: 'id过长会自动收缩',
        ellipsis: true,
        width: '15%'
      },
      {
        title: '创建时间',
        sorter: true,
        dataIndex: 'createTime',
        valueType: 'dateTime',
        hideInSearch: true,
        hideInForm: true,
      },
      {
        title: '更新时间',
        sorter: true,
        dataIndex: 'updateTime',
        valueType: 'dateTime',
        hideInSearch: true,
        hideInForm: true,
      },
      {
        title: '操作',
        dataIndex: 'option',
        valueType: 'option',
        render: (_, date) => (
          <Space size="middle">
            {/* 传递刷新数据函数给删除操作组件 */}
            <ChartForAdminModelFrom item={date} onChartDeleted={handleChartDeleted}/>
          </Space>
        ),
      },
    ];

    return (
      <PageContainer>
        <ProTable<API.Chart>
          headerTitle={'图表列表'}
          rowKey="id"
          dataSource={chartList}
          columns={columns}
          pagination={{
            total,
            pageSize: searchParams.pageSize,
            current: searchParams.current,
            showSizeChanger: true,
            onChange: (page, pageSize) => {
              setSearchParams(prevParams => ({...prevParams, current: page, pageSize}));
            },
            onShowSizeChange: (current, size) => {
              setSearchParams(prevParams => ({...prevParams, current: 1, pageSize: size}));
            },
          }}
        />

      </PageContainer>
    );
  }
;

export default ChartsPage;
